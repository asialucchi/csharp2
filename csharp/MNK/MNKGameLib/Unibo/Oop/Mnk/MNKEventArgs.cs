using System;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk
{
    public abstract class MNKEventArgs
    {
        private IMNKMatch source { get; }
        private  int turn { get; }
        private Symbols player { get; }
        private Tuple<int, int> move { get; }

        public MNKEventArgs(IMNKMatch source, int turn, Symbols player, int i, int j)
        {
            if (!source.Equals(null))
            {
                this.source = source;
            }
            this.turn = turn;
            this.player = player;
            this.move = Tuple.Create(i, j);
        }

    }
}