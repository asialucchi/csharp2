﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk
{
    class MNKMatchImpl : IMNKMatch
    {
        private IMatrix<Symbols> grid;
        private int k;
        private int turn;
        private bool ended = false;
        private Symbols player;
        public MNKMatchImpl(int m, int n, int k) : this(m, n, k, SymbolsExtensions.PickRandomly())
        {
        }

        public MNKMatchImpl(int m, int n, int k, Symbols first)
        {
            if (k > Math.Min(m, n))
            {
                throw new ArgumentException("Required: k <= min(m, n)");
            }
            this.grid = Matrix.Of(m, n, Symbols.EMPTY);
            this.k = k;
            if (!first.Equals(null))
            {
                this.player = first;
            }
            this.turn = 1;
        }

        public Symbols CurrentPlayer => player;

        public IImmutableMatrix<Symbols> Grid => ImmutableMatrix.Wrap(grid);

        public int K => k;

        public int Turn => turn;
       
        public event Action<Exception> ErrorOccurred;
        public event Action<MatchEventArgs> MatchEnded;
        public event Action<IMNKMatch> ResetPerformed;
        public event Action<TurnEventArgs> TurnBeginning;
        public event Action<TurnEventArgs> TurnEnded;

        protected Tuple<int, int, Symbols> KAligned(IEnumerable<Symbols> data)
        {
            IEnumerable< Tuple < int, int, Symbols >> lst = SequenceUtils.Subsequences(data);
            return lst.Where(t => t.Item3 != Symbols.EMPTY).Where(t => t.Item2 >= k).FirstOrDefault(); //ci andrebbe findAny in teoria
        }

        protected Nullable<Symbols> checkVictory(int i, int j, Symbols currentPlayer)
        {
            int d = grid.CoordDiagonal(i, j);
            int a = grid.CoordAntidiagonal(i, j);

            List<IEnumerable<Symbols>> lists = new List<IEnumerable<Symbols>> { grid.GetRow(i),   grid.GetColumn(j), grid.GetDiagonal(d), grid.GetAntidiagonal(a)};
            return lists.Select(e => KAligned(e)).Where(e => e != null).Select(e => e.Item3).FirstOrDefault(); //anche qui ci sarebbe finAny e poi orElse dai optional.empty (il default risolve il secondo problema, ma avremmo bisogno di any al posto di first
        }
        private void beforeMoving(int turn, int i, int j, Symbols currentPlayer)
        {

            if (grid[i,j] != Symbols.EMPTY)
                throw new ArgumentException(String.Format("Cell (%d, %d) is not empty!", i, j));
        }

        protected void onMatchEnded(int currentTurn, int i, int j, Nullable<Symbols> winner)
        {
            MatchEnded.Invoke(new MatchEventArgs(this, currentTurn, this.player, winner, i, j));
        }

        protected void onTurnEnded(int currentTurn, int i, int j, Symbols currentPlayer)
        {
            TurnEnded.Invoke(new TurnEventArgs(this, currentTurn, currentPlayer, i, j));
            turn++;
            player = player.NextPlayer();
            TurnBeginning.Invoke(new TurnEventArgs(this, turn, currentPlayer, i, j));
        }


        private void afterMoved(int turn, int i, int j, Symbols currentPlayer)
        {
            Nullable< Symbols > winner = checkVictory(i, j, currentPlayer);
            if (winner!= null || turn >= grid.Count())
            {
                ended = true;
                onMatchEnded(turn, i, j, winner);
            }
            else
            {
                onTurnEnded(turn, i, j, currentPlayer);
            }
        }
        public void Move(int i, int j)
        {
            try
            {
                if (ended)
                {
                    throw new ArgumentException("Match is over!");
                }
                beforeMoving(turn, i, j, player);
                grid[i, j] = player;
                afterMoved(turn, i, j, player);
            }
            catch (Exception ex)
            {
                ErrorOccurred.Invoke(ex);
            }
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        private class turnEnded
        {
        }
    }
}
