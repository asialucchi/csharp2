﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private readonly object[] items;

        public TupleImpl(object[] args)
        {
            this.items = args;
        }

        public object this[int i] => this.items[i]; //se con un metodo si deve solo ritornare qualcosa (get con una riga) si può usare questa sintassi, non c'è bisogno del return

        public int Length => this.items.Length;

        public object[] ToArray()
        {
            return this.items;
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            foreach (Object obj in items)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            return ((obj != null) && (obj is TupleImpl) && (Enumerable.SequenceEqual(items,((TupleImpl)obj).items)));
        }

        public override string ToString()
        {
            return "(" + String.Join(",", items) + ")";
        }

    }
}
