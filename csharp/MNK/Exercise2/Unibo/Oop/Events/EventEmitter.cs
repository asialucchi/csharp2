﻿using System;
namespace Unibo.Oop.Events
{
    public static class EventEmitter
    {
        public static IEventEmitter<T> Ordered<T>() => new OrderedSourceEventImpl<T>();
        public static IEventEmitter<T> Unordered<T>() => new UnorderedSourceEventImpl<T>();
    }
}
