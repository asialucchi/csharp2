﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class OrderedSourceEventImpl<TArg> : AbsractSourceEventImpl<TArg>
    {
        private List<EventListener<TArg>> eventListeners = new List<EventListener<TArg>>();
        protected override List<EventListener<TArg>> getEventListeners()
        {
            return eventListeners;
        }
    }
}
